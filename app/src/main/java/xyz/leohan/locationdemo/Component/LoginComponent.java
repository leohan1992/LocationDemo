package xyz.leohan.locationdemo.Component;

import dagger.Component;
import xyz.leohan.locationdemo.LoginActivity;
import xyz.leohan.locationdemo.MyScope;
import xyz.leohan.locationdemo.module.LoginActivityModule;

/**
 * Created by leo on 2017/5/17.
 */
@MyScope.LoginScope
@Component(modules = LoginActivityModule.class, dependencies = {AppComponent.class})
public interface LoginComponent {
    void inject(LoginActivity loginActivity);
}
