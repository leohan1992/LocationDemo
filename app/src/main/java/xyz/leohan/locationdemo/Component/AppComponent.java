package xyz.leohan.locationdemo.Component;

import android.content.SharedPreferences;

import javax.inject.Singleton;

import dagger.Component;
import retrofit2.Retrofit;
import xyz.leohan.locationdemo.http.HttpService;
import xyz.leohan.locationdemo.module.AppModule;
import xyz.leohan.locationdemo.module.NetModule;

/**
 * Created by leo on 2017/5/16.
 */

@Singleton
@Component(modules={AppModule.class, NetModule.class})
public interface AppComponent {
    Retrofit getRetrofit();
    SharedPreferences getSharedPreferences();
    HttpService getHttpService();
}
