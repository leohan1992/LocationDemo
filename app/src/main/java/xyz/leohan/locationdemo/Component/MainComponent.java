package xyz.leohan.locationdemo.Component;

import dagger.Component;
import xyz.leohan.locationdemo.MainActivity;
import xyz.leohan.locationdemo.MyScope;
import xyz.leohan.locationdemo.module.MainActivityModule;

/**
 * Created by leo on 2017/5/17.
 */
@MyScope.MainScope
@Component(modules = {MainActivityModule.class}, dependencies = {AppComponent.class})
public interface MainComponent {
    void inject(MainActivity activity);
}
