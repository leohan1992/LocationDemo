package xyz.leohan.locationdemo;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import xyz.leohan.locationdemo.Component.DaggerMainComponent;
import xyz.leohan.locationdemo.base.BaseApplication;
import xyz.leohan.locationdemo.bean.UserBean;
import xyz.leohan.locationdemo.contract.MainContract;
import xyz.leohan.locationdemo.module.MainActivityModule;
import xyz.leohan.locationdemo.presenter.MainPresenter;

public class MainActivity extends AppCompatActivity implements MainContract.View{

    @BindView(R.id.tv_userInfo)
    TextView mTvUserInfo;
    @Inject
    MainPresenter mainPresenter;
    private ProgressDialog mDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        Intent intent = getIntent();
        UserBean bean = intent.getParcelableExtra("userBean");
        mTvUserInfo.setText(bean.getLoginName());
        init();
    }

    private void init() {
        DaggerMainComponent.builder().appComponent(((BaseApplication)getApplication()).getNetComponent())
                .mainActivityModule(new MainActivityModule(this))
                .build()
                .inject(this);
        mDialog=new ProgressDialog(this);
        mDialog.setMessage("下载中");
        mDialog.setMax(100);
        mDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
    }

    @Override
    public void showDialog() {
        if (!mDialog.isShowing()){
            mDialog.show();
        }
    }

    @Override
    public void onProgress(int progress) {
        if (mDialog.isShowing()){
            mDialog.setProgress(progress);
        }
    }

    @Override
    public void dismissDialog() {
        if (mDialog.isShowing()){
            mDialog.dismiss();
        }
    }

    @Override
    public void onSuccess() {
        Toast.makeText(MainActivity.this, "success", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onFailed(String msg) {
        Toast.makeText(MainActivity.this,msg,Toast.LENGTH_SHORT).show();
    }
    public void download(View view){
        String path= Environment.getExternalStorageDirectory().getAbsolutePath()+"/1494988157508.apk";
        mainPresenter.downLoadFile("1494988157508.apk",path);
    }
}
