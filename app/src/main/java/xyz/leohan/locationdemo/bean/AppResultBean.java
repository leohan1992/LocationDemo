package xyz.leohan.locationdemo.bean;

/**
 * Created by Leo on 2017/3/13.
 */
public class AppResultBean<T> {
    // 接收文字数据 msg:成功/失败
    private String msg;
    // 接收boolean类型数据 flag:true/false
    private boolean flag;
    // 接收集合对象数据
    private T results;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public boolean isFlag() {
        return flag;
    }

    public void setFlag(boolean flag) {
        this.flag = flag;
    }

    public T getResults() {
        return results;
    }

    public void setResults(T results) {
        this.results = results;
    }
}
