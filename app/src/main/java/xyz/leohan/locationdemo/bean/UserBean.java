/**
 * 公司名称: 唐山启奥（Shinow）
 * 项目名称: SLEC
 * 版本号  : 1.0
 * 创建时间：2017/3/29 15:57
 * 创建人：leo
 **/
package xyz.leohan.locationdemo.bean;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Date;

/**
 * 类说明：
 *
 * @author leo
 * @version Version 1.0
 * @project SLEC
 * @date 2017/3/29.
 */
public class UserBean implements Parcelable {

    /**
     * http://10.1.2.179:8081/rest/shinowService/userLogin?name=aaa&pwd=aaaaaa
     * loginName : aaa
     * orgEditPeople : aaa
     * orgEditTime : 1488199837000
     * orgId : 2
     * orgName : 唐山市
     * password : 4cc0e3687da5d31e0d6eae8f8687ffb9
     * roleId : 14AB859776E24EA1B05F3E513D24073E
     * roleName : 123
     * userCode : 3123
     * userCreatePeople : aaa
     * userCreateTime : 1487852077000
     * userFlag : 1
     * userId : 324A0568102B45DFB46A74E3A9165E4E
     * userIden : 123112312312313
     * userName : 12312
     * userStatus : 1
     */

    private String loginName;
    private String orgEditPeople;
    private Date orgEditTime;
    private String orgId;
    private String orgName;
    private String password;
    private String roleId;
    private String roleName;
    private String userCode;
    private String userCreatePeople;
    private Date userCreateTime;
    private int userFlag;
    private String userId;
    private String userIden;
    private String userName;
    private int userStatus;

    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    public String getOrgEditPeople() {
        return orgEditPeople;
    }

    public void setOrgEditPeople(String orgEditPeople) {
        this.orgEditPeople = orgEditPeople;
    }

    public Date getOrgEditTime() {
        return orgEditTime;
    }

    public void setOrgEditTime(Date orgEditTime) {
        this.orgEditTime = orgEditTime;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getUserCode() {
        return userCode;
    }

    public void setUserCode(String userCode) {
        this.userCode = userCode;
    }

    public String getUserCreatePeople() {
        return userCreatePeople;
    }

    public void setUserCreatePeople(String userCreatePeople) {
        this.userCreatePeople = userCreatePeople;
    }


    public Date getUserCreateTime() {
        return userCreateTime;
    }

    public void setUserCreateTime(Date userCreateTime) {
        this.userCreateTime = userCreateTime;
    }

    public int getUserFlag() {
        return userFlag;
    }

    public void setUserFlag(int userFlag) {
        this.userFlag = userFlag;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserIden() {
        return userIden;
    }

    public void setUserIden(String userIden) {
        this.userIden = userIden;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public int getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(int userStatus) {
        this.userStatus = userStatus;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.loginName);
        dest.writeString(this.orgEditPeople);
        dest.writeLong(this.orgEditTime != null ? this.orgEditTime.getTime() : -1);
        dest.writeString(this.orgId);
        dest.writeString(this.orgName);
        dest.writeString(this.password);
        dest.writeString(this.roleId);
        dest.writeString(this.roleName);
        dest.writeString(this.userCode);
        dest.writeString(this.userCreatePeople);
        dest.writeLong(this.userCreateTime != null ? this.userCreateTime.getTime() : -1);
        dest.writeInt(this.userFlag);
        dest.writeString(this.userId);
        dest.writeString(this.userIden);
        dest.writeString(this.userName);
        dest.writeInt(this.userStatus);
    }

    public UserBean() {
    }

    protected UserBean(Parcel in) {
        this.loginName = in.readString();
        this.orgEditPeople = in.readString();
        long tmpOrgEditTime = in.readLong();
        this.orgEditTime = tmpOrgEditTime == -1 ? null : new Date(tmpOrgEditTime);
        this.orgId = in.readString();
        this.orgName = in.readString();
        this.password = in.readString();
        this.roleId = in.readString();
        this.roleName = in.readString();
        this.userCode = in.readString();
        this.userCreatePeople = in.readString();
        long tmpUserCreateTime = in.readLong();
        this.userCreateTime = tmpUserCreateTime == -1 ? null : new Date(tmpUserCreateTime);
        this.userFlag = in.readInt();
        this.userId = in.readString();
        this.userIden = in.readString();
        this.userName = in.readString();
        this.userStatus = in.readInt();
    }

    public static final Parcelable.Creator<UserBean> CREATOR = new Parcelable.Creator<UserBean>() {
        @Override
        public UserBean createFromParcel(Parcel source) {
            return new UserBean(source);
        }

        @Override
        public UserBean[] newArray(int size) {
            return new UserBean[size];
        }
    };
}
