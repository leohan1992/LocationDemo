package xyz.leohan.locationdemo;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import xyz.leohan.locationdemo.Component.DaggerLoginComponent;
import xyz.leohan.locationdemo.base.BaseApplication;
import xyz.leohan.locationdemo.bean.UserBean;
import xyz.leohan.locationdemo.contract.LoginContract;
import xyz.leohan.locationdemo.module.LoginActivityModule;
import xyz.leohan.locationdemo.presenter.LoginPresenter;

/**
 * Created by leo on 2017/5/10.
 */

public class LoginActivity extends AppCompatActivity implements LoginContract.View {
    @BindView(R.id.et_userName)
    EditText mEtUserName;
    @BindView(R.id.et_password)
    EditText mEtPassword;
    @Inject
    LoginPresenter presenter;
    private ProgressDialog mProgressDialog;
    private Context mContext;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = this;
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        init();
    }

    private void init() {
        DaggerLoginComponent.builder()
                .appComponent(((BaseApplication) getApplication()).getNetComponent())
                .loginActivityModule(new LoginActivityModule(this))
                .build().inject(this);
        mProgressDialog = new ProgressDialog(mContext);
        mProgressDialog.setMessage("请稍后...");

    }

    public void loginClick(View view) {
        presenter.doLoginRx(mEtUserName.getText().toString(), mEtPassword.getText().toString());
    }

    @Override
    public void onLoginSuccess(UserBean bean) {
        Intent intent = new Intent();
        intent.putExtra("userBean", bean);
        intent.setClass(mContext, MainActivity.class);
        startActivity(intent);
    }

    @Override
    public void onLoginFailed(String msg) {
        Toast.makeText(mContext, msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showDialog() {
        if (!mProgressDialog.isShowing())
            mProgressDialog.show();
    }

    @Override
    public void dismissDialog() {
        if (mProgressDialog.isShowing())
            mProgressDialog.dismiss();
    }
}
