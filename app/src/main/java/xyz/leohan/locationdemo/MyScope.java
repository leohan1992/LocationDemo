package xyz.leohan.locationdemo;

import javax.inject.Scope;

/**
 * Created by leo on 2017/5/17.
 */

public class MyScope {
    @Scope
    public @interface LoginScope {

    }
    @Scope
    public @interface MainScope {

    }
}
