package xyz.leohan.locationdemo.base;

import android.app.Application;

import xyz.leohan.locationdemo.Component.AppComponent;
import xyz.leohan.locationdemo.Component.DaggerAppComponent;
import xyz.leohan.locationdemo.module.AppModule;
import xyz.leohan.locationdemo.module.NetModule;

/**
 * Created by leo on 2017/5/16.
 */

public class BaseApplication extends Application {
    private AppComponent mAppComponent;
    @Override
    public void onCreate() {
        super.onCreate();
        mAppComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .netModule(new NetModule())
                .build();
        // If a Dagger 2 component does not have any constructor arguments for any of its modules,
        // then we can use .create() as a shortcut instead:
        //  mAppComponent = com.codepath.dagger.components.DaggerAppComponent.create();
    }

    public AppComponent getNetComponent() {
        return mAppComponent;
    }
}
