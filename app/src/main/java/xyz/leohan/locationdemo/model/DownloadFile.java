package xyz.leohan.locationdemo.model;

import javax.inject.Inject;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Retrofit;
import xyz.leohan.locationdemo.http.HttpDownloadCallBack;
import xyz.leohan.locationdemo.http.HttpService;

/**
 * Created by leo on 2017/5/17.
 */

public class DownloadFile {
    @Inject
    Retrofit retrofit;
    @Inject
    public DownloadFile(){

    }
    public void downloadFile(String appName, HttpDownloadCallBack callBack){
        HttpService httpService = retrofit.create(HttpService.class);
        Call<ResponseBody> call = httpService.download(appName);
        call.enqueue(callBack);
    }
}
