package xyz.leohan.locationdemo.model;


import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Call;
import xyz.leohan.locationdemo.bean.AppResultBean;
import xyz.leohan.locationdemo.bean.UserBean;
import xyz.leohan.locationdemo.http.HttpCallBack;
import xyz.leohan.locationdemo.http.HttpService;

/**
 * Created by leo on 2017/5/16.
 */

public class UserLoginNet {
//    @Inject
//    Retrofit mRetrofit;
    @Inject
    HttpService httpService;
    @Inject
    public UserLoginNet() {
    }
//
    public void doLogin(String userName, String password, HttpCallBack<AppResultBean<UserBean>> callBack) {
        Call<AppResultBean<UserBean>> call = httpService.login(userName, password);
        call.enqueue(callBack);
    }

    public void doLoginRx(String userName, String password,Observer<AppResultBean<UserBean>> subscriber){
      Observable<AppResultBean<UserBean>> observable= httpService.loginRx(userName,password);
        observable.subscribeOn(Schedulers.newThread())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(subscriber);
    }
}
