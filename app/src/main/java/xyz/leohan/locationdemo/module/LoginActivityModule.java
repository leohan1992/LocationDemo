package xyz.leohan.locationdemo.module;

import dagger.Module;
import dagger.Provides;
import xyz.leohan.locationdemo.LoginActivity;
import xyz.leohan.locationdemo.contract.LoginContract;

/**
 * Created by leo on 2017/5/16.
 */
@Module
public class LoginActivityModule {
    LoginActivity mLoginActivity;

    public LoginActivityModule(LoginActivity loginActivity) {
        this.mLoginActivity = loginActivity;
    }

    @Provides
    LoginActivity provideLoginActivity() {
        return mLoginActivity;
    }

    @Provides
    LoginContract.View provideLoginView() {
        return mLoginActivity;
    }

}
