package xyz.leohan.locationdemo.module;

import dagger.Module;
import dagger.Provides;
import xyz.leohan.locationdemo.MainActivity;
import xyz.leohan.locationdemo.contract.MainContract;

/**
 * Created by leo on 2017/5/17.
 */
@Module
public class MainActivityModule {
    MainActivity mMainActivity;
    public MainActivityModule(MainActivity activity){
        this.mMainActivity=activity;
    }
    @Provides
    MainActivity provideMainActivity(){
        return mMainActivity;
    }
    @Provides
    MainContract.View provideMainView(){
        return mMainActivity;
    }
}
