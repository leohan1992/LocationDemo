package xyz.leohan.locationdemo.http;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by leo on 2017/5/16.
 * 普通http回调
 */
public abstract class HttpCallBack<T> implements Callback<T> {
    public abstract void onSuccess(T T);

    public abstract void onFailed(String msg);

    @Override
    public void onResponse(Call<T> call, Response<T> response) {
        onSuccess(response.body());
    }

    @Override
    public void onFailure(Call<T> call, Throwable t) {
        onFailed(t.getMessage());
    }
}
