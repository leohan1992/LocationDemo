package xyz.leohan.locationdemo.http;

import io.reactivex.Observable;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Streaming;
import xyz.leohan.locationdemo.bean.AppResultBean;
import xyz.leohan.locationdemo.bean.UserBean;

/**
 * Created by leo on 2017/5/16.
 */

public interface HttpService {
    @POST("rest/app/login")
    @FormUrlEncoded
    Call<AppResultBean<UserBean>> login(@Field("userName") String userName, @Field("password") String password);

    @GET("static/slec/apk/{apkName}")
    @Streaming
    Call<ResponseBody> download(@Path("apkName") String apkName);

    @POST("rest/app/login")
    @FormUrlEncoded
    Observable<AppResultBean<UserBean>> loginRx(@Field("userName") String userName, @Field("password") String password);
}
