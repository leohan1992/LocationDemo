package xyz.leohan.locationdemo.contract;

import xyz.leohan.locationdemo.bean.UserBean;

/**
 * Created by leo on 2017/5/17.
 */

public interface LoginContract {
    interface View{
        void onLoginSuccess(UserBean bean);
        void onLoginFailed(String msg);
        void showDialog();
        void dismissDialog();
    }

    interface Presenter{
        void doLogin(String userName, String password);
        void doLoginRx(String userName, String password);
    }
}
