package xyz.leohan.locationdemo.contract;

/**
 * Created by leo on 2017/5/17.
 */

public interface MainContract {
    interface View{
        void showDialog();
        void onProgress(int progress);
        void dismissDialog();
        void onSuccess();
        void onFailed(String msg);
    }
    interface Presenter{
        void downLoadFile(String apkUrl, String fileSavePath);
    }
}
