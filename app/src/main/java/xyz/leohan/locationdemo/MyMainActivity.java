package xyz.leohan.locationdemo;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by leo on 2017/5/27.
 */

public class MyMainActivity extends AppCompatActivity {
    private static final int REQUEST_CODE_CAPTURE = 1;
    @BindView(R.id.tv_resultContent)
    TextView mTvResultContent;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mymain);
        ButterKnife.bind(this);

    }

    public void startScan(View view) {
        Intent intent = new Intent(MyMainActivity.this, CaptureActivity.class);
        startActivityForResult(intent, REQUEST_CODE_CAPTURE);
    }

    public void startNFC(View view) {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_CAPTURE) {
            if (resultCode == Activity.RESULT_OK) {
                String result = data.getStringExtra("rawResult");
                mTvResultContent.setText(result);
            }
        }
    }
}
