package xyz.leohan.locationdemo.presenter;

import javax.inject.Inject;

import xyz.leohan.locationdemo.contract.MainContract;
import xyz.leohan.locationdemo.http.HttpDownloadCallBack;
import xyz.leohan.locationdemo.model.DownloadFile;

/**
 * Created by leo on 2017/5/17.
 */

public class MainPresenter implements MainContract.Presenter {
    @Inject
    DownloadFile downloadFile;
    @Inject
    MainContract.View mView;

    @Inject
    public MainPresenter() {

    }

    @Override
    public void downLoadFile(String apkUrl, String fileSavePath) {
        mView.showDialog();
        downloadFile.downloadFile(apkUrl, new HttpDownloadCallBack(fileSavePath) {
            @Override
            public void onFailed(String msg) {
                mView.dismissDialog();
                mView.onFailed(msg);
            }

            @Override
            public void onProgress(boolean done, long completeSize, long total) {
                int progress = (int) ((completeSize * 100) / total);
                mView.onProgress(progress);
                if (done) {
                    mView.dismissDialog();
                    mView.onSuccess();
                }
            }
        });
    }
}
