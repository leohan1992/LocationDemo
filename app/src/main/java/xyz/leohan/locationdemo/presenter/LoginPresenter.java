package xyz.leohan.locationdemo.presenter;

import android.util.Log;

import javax.inject.Inject;

import io.reactivex.Observer;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import xyz.leohan.locationdemo.bean.AppResultBean;
import xyz.leohan.locationdemo.bean.UserBean;
import xyz.leohan.locationdemo.contract.LoginContract;
import xyz.leohan.locationdemo.http.HttpCallBack;
import xyz.leohan.locationdemo.model.UserLoginNet;

/**
 * Created by leo on 2017/5/17.
 */

public class LoginPresenter implements LoginContract.Presenter {
    @Inject
    LoginContract.View mView;
    @Inject
    UserLoginNet mUserLoginNet;

    @Inject
    public LoginPresenter() {

    }

    @Override
    public void doLogin(String userName, String password) {
        mView.showDialog();
        mUserLoginNet.doLogin(userName, password, new HttpCallBack<AppResultBean<UserBean>>() {
            @Override
            public void onSuccess(AppResultBean<UserBean> bean) {
                mView.dismissDialog();
                if (bean.isFlag()) {
                    mView.onLoginSuccess(bean.getResults());
                } else {
                    onFailed(bean.getMsg());
                }
            }

            @Override
            public void onFailed(String msg) {
                mView.dismissDialog();
                mView.onLoginFailed(msg);
            }
        });
    }

    @Override
    public void doLoginRx(String userName, String password) {
        mView.showDialog();
        mUserLoginNet.doLoginRx(userName, password, new Observer<AppResultBean<UserBean>>() {

            @Override
            public void onSubscribe(@NonNull Disposable d) {

            }

            @Override
            public void onNext(AppResultBean<UserBean> bean) {
                Log.i("rxjava","next");
                mView.dismissDialog();
                if (bean.isFlag()) {
                    mView.onLoginSuccess(bean.getResults());
                }
            }

            @Override
            public void onError(Throwable t) {
                Log.i("rxjava","onError----->"+t.getMessage());
            }

            @Override
            public void onComplete() {
                Log.i("rxjava","complete----->");
            }
        });
    }
}
